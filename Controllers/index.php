<?php

class Index extends Controller {

    function __construct() {
        parent::__construct();
        $this->view->css = array('index/css/main.css');
        $this->view->js = array('index/js/Main.js');
    }

    function index() {
        $this->view->title = 'Home';
        $this->view->render('index/index');
    }

}
