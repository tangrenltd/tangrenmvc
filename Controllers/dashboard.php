<?php

class dashboard extends Controller {

    function __construct() {
        parent::__construct();
//        $this->view->css = array('login/css/main.css');
//        $this->view->js = array('login/js/Main.js');

        $this->view->dash = "";
        $this->view->edit = "";
        $this->view->msg = "";
        $this->view->msgin = "";
        $this->view->fri = "";
        $this->view->perm = "";
        $this->view->buypoints = "";
        $this->view->design = "";
    }

    function index() {
        $this->view->title = 'Dashboard';
        $this->view->dash = "active";
        $this->view->theme('dashboard/index');
    }

    function edit() {
        $this->view->title = 'edit';
        $this->view->edit = "active";
        $this->view->theme('dashboard/edit');
    }

    function message() {
        $this->view->title = 'message';
        $this->view->msg = "active";
        $this->view->msgin = "in";
        $this->view->theme('dashboard/message');
    }

    function friends() {
        $this->view->css = array('dashboard/css/friends.css');
        $this->view->title = 'friends';
        $this->view->fri = "active";
        $this->view->theme('dashboard/friends');
    }

    function permissions() {
        $this->view->title = 'permissions';
        $this->view->perm = "active";
        $this->view->theme('dashboard/permissions');
    }

    function buypoints() {
        $this->view->title = 'buypoints';
        $this->view->buypoints = "active";
        $this->view->theme('dashboard/buypoints');
    }

    function designcard() {
        $this->view->title = 'designcard';
        $this->view->design = "active";
        $this->view->theme('dashboard/designcard');
    }

}
