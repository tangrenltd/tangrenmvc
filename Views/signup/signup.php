<div class="col-lg-3">

</div>
<div class="col-lg-6">
    <form class="form-horizontal"id="signupform" >
        <fieldset>

            <!-- Form Name -->
            <legend>Sign Up</legend>
            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="inpuser">Username:</label>  
                <div class="col-md-5">
                    <input id="inpuser" name="warnuser" type="text" placeholder="Username..." class="form-control input-md" required="" onfocus="cleanwarn(this.name)"  minlength="6" maxlength="12">
                    <span id="warnuser"class="help-block">*required</span>
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="inppass">Password:</label>
                <div class="col-md-5">
                    <input id="inppass" name="inppass" type="password" placeholder="Password..." class="form-control input-md" required="" minlength="6" maxlength="12">
                    <span class="help-block">*required</span>
                </div>
            </div>

            <!-- Password input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="inppass1">Confirm Password:</label>
                <div class="col-md-5">
                    <input id="inppass1" name="warnpass" type="password" placeholder="Password..." class="form-control input-md" required="" onfocus="cleanwarn(this.name)" minlength="6" maxlength="12">
                    <p class="help-block" id="warnpass">*required</p>
                </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="inpemail">Email:</label>  
                <div class="col-md-5">
                    <input id="inpemail" name="warnemail" type="text" placeholder="Email..." class="form-control input-md" onfocus="cleanwarn(this.name)" required="">
                    <p class="help-block" id="warnemail">*required</p>  
                </div>
            </div>

            <!-- Textarea -->
            <div class="form-group">
                <label class="col-md-4 control-label" for="textarea">Agreement</label>
                <div class="col-md-5">                     
                    <textarea class="form-control textarea" id="textarea" name="textarea" disabled="disabled" style="width: 240px;height: 250px;">zxcvjkasdapomlfka
                    asdkljasldjasldkjas</textarea>
                </div>
            </div>

            <!-- Button -->
            <div class="form-group">
                <div class="col-md-4 pull-right">
                    <input type="submit" id="singlebutton" name="singlebutton" class="btn btn-primary"></input>
                </div>
            </div>
        </fieldset>
    </form>
    <script>
//        $('#inpuser').on('click', function () {
//             $("#warnuser").html("Username must be..");
//                    document.getElementById("warnuser").style.color = "red";
//                }

        function cleanwarn(id) {
            document.getElementById(id).innerHTML = "*required";
            document.getElementById(id).style.color = "Gray";
        }

        function validateEmail(email) {
            var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            return re.test(email);
        }

        $('#signupform').on('submit', function () {
//            var values = $('#signupform').serialize();
            var email = $('#inpemail').val(),
                    pass1 = $('#inppass').val(),
                    pass2 = $('#inppass1').val();

//            var myform_return = function (email) {
//            alert(email);
            if (pass1 == pass2) {
                if (validateEmail(email) == true) {
                    alert("post form here");
                } else {
                    document.getElementById("warnemail").innerHTML = "email is not vaild";
                    document.getElementById("warnemail").style.color = "red";
                    return false;
                }
            } else {
                $("#warnpass").html("password is not same");
                document.getElementById("warnpass").style.color = "red";
                return false;
            }
            return false;
        });

    </script>


    <div class="col-lg-3">

    </div>
