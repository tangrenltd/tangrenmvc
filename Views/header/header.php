<nav class="navbar navbar-default header1">
    <div class="col-xs-12">
        <h4 class="welco">Welcome to icorp.co.nz!</h4>
        <!-- Button trigger modal -->
        <div>
            <a type="button" class="btn pull-right signin" data-toggle="modal" data-target="#myModal">
                Sign In
            </a>

            <!-- Modal -->
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Membership</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label for="exampleInputName2">Username</label>
                                    <input type="text" class="form-control" id="username" placeholder="Username">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" class="form-control" id="password" placeholder="Password">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> Remember Login
                                    </label>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a href="<?php echo URL; ?>dashboard" class="btn btn-primary" onclick="">Login</a>
                        </div>

                        <script>

                        </script>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <a href ="<?php echo URL;?>signup" type="button" class="btn pull-right signin">
                Sign Up
            </a>
        </div>
    </div>
</nav>

