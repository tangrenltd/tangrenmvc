<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="<?php echo URL; ?>public/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL; ?>public/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL; ?>public/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo URL; ?>public/css/main.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo URL; ?>public/js/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="<?php echo URL; ?>public/js/bootstrap.js" type="text/javascript"></script>
        <?php
        if (isset($this->css)) {
            foreach ($this->css as $css) {
                echo '<link href="' . URL . 'Views/' . $css . '" rel="stylesheet" type="text/css"/>';
            }
        }
        ?>
        <?php
        if (isset($this->js)) {
            foreach ($this->js as $js) {
                echo '<script src="' . URL . 'Views/' . $js . '" type="text/javascript"></script>';
            }
        }
        ?>
        <title><?php echo $this->title; ?></title>
    </head>
    <body>
        <div class="container">
            <header>
                <?php require_once 'header.php'; ?>
                <?php require_once 'logo.php'; ?>
            </header>