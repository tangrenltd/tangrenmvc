<HR width="80%" color=#987cb9 SIZE=3>
<div class="container">
    <div class="col-xs-3">
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-success">Dapibus ac facilisis in</a>
            <a href="#" class="list-group-item list-group-item-success">Cras sit amet nibh libero</a>
            <a href="#" class="list-group-item list-group-item-success">Porta ac consectetur ac</a>
            <a href="#" class="list-group-item list-group-item-success">Vestibulum at eros</a>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-info">Dapibus ac facilisis in</a>
            <a href="#" class="list-group-item list-group-item-info">Cras sit amet nibh libero</a>
            <a href="#" class="list-group-item list-group-item-info">Porta ac consectetur ac</a>
            <a href="#" class="list-group-item list-group-item-info">Vestibulum at eros</a>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-warning">Dapibus ac facilisis in</a>
            <a href="#" class="list-group-item list-group-item-warning">Cras sit amet nibh libero</a>
            <a href="#" class="list-group-item list-group-item-warning">Porta ac consectetur ac</a>
            <a href="#" class="list-group-item list-group-item-warning">Vestibulum at eros</a>
        </div>
    </div>
    <div class="col-xs-3">
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-danger">Dapibus ac facilisis in</a>
            <a href="#" class="list-group-item list-group-item-danger">Cras sit amet nibh libero</a>
            <a href="#" class="list-group-item list-group-item-danger">Porta ac consectetur ac</a>
            <a href="#" class="list-group-item list-group-item-danger">Vestibulum at eros</a>
        </div>
    </div>
</div>