<div class="col-md-12">
    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="pill" href="#personal">Personal</a></li>
        <li><a data-toggle="pill" href="#public">Public</a></li>
        <li class="pull-right margin5"><a href="#" class="glyphicon glyphicon-cog" aria-hidden="true"></a></li>
    </ul>

    <div class="tab-content">
        <div id="personal" class="tab-pane fade in active">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <td style="width: 150px;"><b>#</b></td>
                        <td><b>Title</b></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>asdasad</td>
                        <td class='pull-right margin5'><a href="#" class="glyphicon glyphicon-trash" aria-hidden="true"></a></td>
                    </tr>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>srgtyhjk</td>
                    </tr>
                </tbody>

            </table>
        </div>
        <div id="public" class="tab-pane fade">
            <h3>Menu 1</h3>
            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        </div>
    </div>
</div>