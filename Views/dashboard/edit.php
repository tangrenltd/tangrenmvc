<div class="col-md-12">
    <div class="col-md-4">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
            <tr>
                <td class='titleli'><b>First Name:</b></td>
                <td class='contentli'>.............</td>
            </tr>
            <tr>
                <td class='titleli'><b>Last Name:</b></td>
                <td class='contentli'>.............</td>
            </tr>
            <tr>
                <td class='titleli'><b>Email:</b></td>
                <td class='contentli'>.............</td>
            </tr>
            <tr>
                <td class='titleli'><b>Phone:</b></td>
                <td class='contentli'>.............</td>
            </tr>
        </table>
    </div>
    <div class="col-md-4">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr></tr>
            </thead>
        </table>
    </div>
    <div class="col-md-4 pull-right">
        <div class="panel panel-default marginall">
            <table class="marginall">
                <tr>
                    <td><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></td>
                    <td class="fontpadding"><a href='#'>Edit Content</a></td>
                </tr>
                <tr>
                    <td><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></td>
                    <td class="fontpadding"><a href='#'>Change Password</a></td>
                </tr>
                <tr>
                    <td><span class="glyphicon glyphicon-picture" aria-hidden="true"></span></td>
                    <td class="fontpadding"><a href="#">Change Email</a></td>
                </tr>
            </table>
        </div>
    </div>
</div>