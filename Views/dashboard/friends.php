<div class="col-md-12">
    <div class="friendbg">

    </div>
    <div class="container-fluid borderstyle margin-bottom friendsList" style="width: 770px; height: 470px; overflow: auto; border-radius: 10px;">

        <?php for ($index = 0; $index < 10; $index++): ?>
            <div class="col-md-2 friend">
                <div class="summary">
                    <div class="friendimg">
                        <img class="img-responsive" src="http://placehold.it/110x110" alt="">
                    </div>
                    <div class="friendname">
                        <p>fiends_<?php echo $index; ?></p>
                    </div>
                </div>
                <div class="friendinfo">
                    <div class="info name">
                        <div class="infolabel">name: </div>
                        <div class="infodetail">dsadsad dsadsa</div>
                    </div>
                    <div class="info name">
                        <div class="infolabel">email: </div>
                        <div class="infodetail">dsadsad dsadsa.com</div>
                    </div>
                    <div class="info name">
                        <div class="infolabel">phone: </div>
                        <div class="infodetail">456756789056</div>
                    </div>
                    <div class="info name">
                        <div class="infolabel">address: </div>
                        <div class="infodetail">34vdf t54 56  54  54</div>
                    </div>
                    <div class="info name">
                        <div class="infolabel">age: </div>
                        <div class="infodetail">55</div>
                    </div>
                    <div class="info name">
                        <div class="infolabel">content: </div>
                        <div class="infodetail">dsadsad dsadsa dsdsad asdsadsadsadas dsadsa asdsadsasa asdscdsdsd ed fcdscvdfsvfdvdfvf  dfvdfs d</div>
                    </div>
                </div>
            </div>
        <?php endfor; ?>
        <script>
            $('.friend').on("click", function ()
            {
                var html = '<button type="button" class="close" data-label="Close"><span aria-hidden="true">×</span></button>' + $(this).html();
                $('.friendbg').html(html);
                $('.friendbg').css('height', "460px");
                $('.friendbg').css('opacity', "1");

                $('button.close[data-label="Close"]').on("click", function () {
                    $('.friendbg').html('');
                    $('.friendbg').css('opacity', 0);
                    $('.friendbg').css('height', "0px");
                });
            });

            $(document).ready(function () {
                $('[data-toggle="popover"]').popover();
            });
        </script>

    </div>
</div>