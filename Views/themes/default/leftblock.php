<div class="col-md-4">
    <a href="<?php echo URL; ?>dashboard"><img src="http://placehold.it/60x60" alt="Avatar" class="thumbnail border"></a>
</div>
<div class="col-md-8">
    <table class="marginall">
        <tr>
            <td><span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
            <td class="fontpadding">username</td>
        </tr>
        <tr>
            <td><span class="glyphicon glyphicon-flash" aria-hidden="true"></span></td>
            <td class="fontpadding">points</td>
        </tr>
    </table>
</div>
<div class="col-md-12">
    <ul class="nav nav-pills nav-stacked">
        <li role="presentation" class="<?php echo $this->dash;?>"><a href="<?php echo URL; ?>dashboard" >Dashboard</a></li>
        <li role="presentation" class="<?php echo $this->edit;?>"><a href="<?php echo URL; ?>dashboard/edit" >Edit Profile</a></li>
        <li role="presentation" class="<?php echo $this->msg;?>"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Message<span class="badge">4</span></a></li>
        <div id="collapseOne" class="accordion-body collapse <?php echo $this->msgin;?>">
            <div class="accordion-inner">
                <ul class="list-unstyled" style="margin-left: 30px;">
                    <li><a href="<?php echo URL; ?>dashboard/message"><span class="glyphicon glyphicon-envelope" style="margin-right: 5px;"></span>Messages</a></li>
                    <li><a href="<?php echo URL; ?>dashboard/message"><span class="glyphicon glyphicon-alert" style="margin-right: 5px;"></span>System</a></li>
                </ul>
            </div>
        </div>
        <li role="presentation" class="<?php echo $this->fri;?>"><a href="<?php echo URL; ?>dashboard/friends">Friends</a></li>
        <li role="presentation" class="<?php echo $this->perm;?>"><a href="<?php echo URL; ?>dashboard/permissions">Permissions</a></li>
        <li role="presentation" class="<?php echo $this->buypoints;?>"><a href="<?php echo URL; ?>dashboard/buypoints">Buy Points</a></li>
        <li role="presentation" class="<?php echo $this->design;?>"><a href="<?php echo URL; ?>dashboard/designcard">Design Your Own Business Card</a></li>
    </ul>
</div>
            