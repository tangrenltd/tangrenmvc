<?php

class View {

    public function __construct() {
        
    }

    public function render($name, $defaultNav = true, $noInclude = false) {
        if ($noInclude) {
            if ($defaultNav) {
                require 'Views/Nav/index.php';
                require 'Views/' . $name . '.php';
            } else {
                require 'Views/' . $name . '.php';
            }
        } else {
            if ($defaultNav) {
                require 'Views/header/index.php';
                require 'Views/Nav/index.php';
                require 'Views/' . $name . '.php';
                require 'Views/footer/footer.php';
            } else {
                require 'Views/header/index.php';
                require 'Views/' . $name . '.php';
                require 'Views/footer/footer.php';
            }
        }
    }

    public function theme($index, $theme = "default", $defaultNav = true, $noInclude = false) {
        $this->render = $index;
        $title = explode("/", $index);
        if ($title[1] == "index") {
            $this->nav = array("dashboard");
        } else {
            $this->nav=array($title[1]);
        }

        require 'Views/header/index.php';
        require 'Views/Nav/index.php';
        require 'Views/themes/' . $theme . '/index.php';
        require 'Views/footer/footer.php';
    }

}
